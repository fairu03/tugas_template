<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/table', function () {
//     return view('items.table');
// });
// Route::get('/data-table', function(){
//     return view('items.data-table');
// });
Route::get('/user', 'UsersController@index');
Route::get('/user/create', 'UsersController@create');
Route::post('/user', 'UsersController@store');
Route::get('/user/{id}', 'UsersController@show');
Route::get('/user/{id}/edit', 'UsersController@edit');
Route::put('/user/{id}', 'UsersController@update');
Route::delete('/user/{id}', 'UsersController@destroy');
// Route::get('/profile/create', 'ProfileController@create');
// Route::post('/profile', 'ProfileController@store');
