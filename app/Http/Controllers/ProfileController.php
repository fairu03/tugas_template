<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProfileController extends Controller
{
    public function create(){
        return view('users.create');
    }
    public function store(Request $request){

        // $request->validate([
        //     "name"=> "request"
        // ])
        // dd($request->all());
        $data = DB::table('users')->insert([
            "name" => $request["name"],
            "email" => $request["email"],
            "password" => $request["password"]
            
        ]);
        return redirect('/users/create');

    }
}
