<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UsersController extends Controller
{
    public function index ()
    {
        $data_user = DB::table('users')->get();
        return view('users.index', compact('data_user'));
    }    
    public function create(){
        return view('users.create');
    }
    public function store(Request $request){

        $request->validate([
            "name"=> 'required',
            "email"=> 'required',
            "password"=> 'required',

        ]);
        
        // dd($request->all());
        $data_user = DB::table('users')->insert([
            "name" => $request["name"],
            "email" => $request["email"],
            "password" => $request["password"]
            
        ]);
        return redirect('/user');
    }
    
        public function show($id)
        {
            
            $data_user = DB::table('users')->where('id', $id)->first();
            return view('users.show', compact('data_user'));
        }



        public function edit($id)
        {
            $data_user = DB::table('users')->where('id', $id)->first();
            return view('users.edit', compact('data_user'));
        }
    



        public function update($id, Request $request)
        {
            $request->validate([
                'name' => 'required',
                'email' => 'required',
                'password' => 'required',

            ]);
    
            DB::table('users')
                ->where('id', $id)
                ->update([
                    'name' => $request["name"],
                    'email' => $request["email"],
                    'password' => $request["password"],
                ]);
            return redirect('/user');
        }
        
            public function destroy($id)
            {
                DB::table('users')->where('id', $id)->delete();
                return redirect('/user');
            }
}